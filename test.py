import cv2
import numpy as np
from pyzbar.pyzbar import decode

cap = cv2.VideoCapture('129941.t.mp4')
font = cv2.FONT_HERSHEY_PLAIN

while True:
    sucess, frame = cap.read()


    for barcode in decode(frame):
        myData = barcode.data.decode('utf-8')
        print(myData)
        pts = np.array([barcode.polygon] ,np.int32)
        pts = pts.reshape((-1,1,2))
        cv2.polylines(frame,[pts],True,(255,0,255),5)
    cv2.imshow("Frame", frame)
    cv2.waitKey(1)

